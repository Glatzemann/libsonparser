#pragma once

#include "GenericValue.h"

#include <string>
#include <vector>
#include <stack>

enum class ParserState;

class SonParser
{
public:
	SonParser();
	SonParser(const std::string& source);
	SonParser(const char* source, size_t& length);

	void SetSource(const std::string& source);
	void SetSource(const char* source, size_t& length);

	void Parse(GenericValue& document);
	void Parse(GenericValue& document, const std::string& source);
	void Parse(GenericValue& document, const char* source, size_t& length);

	bool HasParseErrors();
	std::string GetParseError();

private:
	void ResetError();
	void SetError(const char* error, size_t& position);
	void SetError(std::string& error, size_t& position);

	void Parse(GenericValue& document, std::stack<ParserState>& stateStack);

private:
	const char* source;
	size_t length;
	bool hasParseErrors;
	std::string parseError;
};
