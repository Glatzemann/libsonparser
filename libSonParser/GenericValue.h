#pragma once

#include <string>
#include <vector>

class GenericValue;

enum class GenericValueType
{
	Document,
	Object,
	NamedObject,
	Value,
	KeyValue,
	KeyValueArray,
};

class GenericValue : public std::vector<GenericValue*>
{
public:
	GenericValue(GenericValueType type) : type(type) {}

	~GenericValue()
	{
		//TODO: cleanup
		//for (size_t i = this->size()-1; i >= 0; i--)
		//{
		//	delete this->at(i);
		//}
	}

	GenericValueType GetType() { return type; }

	std::string getStringValue(std::string key, std::string defaultValue = "");
	int getIntValue(std::string key, int defaultValue = 0);
	float getFloatValue(std::string key, float defaultValue = 0.0f);
	double getDoubleValue(std::string key, double defaultValue = 0.0);
	std::vector<GenericValue*> getObjectArray(std::string key);

private:
	GenericValueType type;
};

class Object : public GenericValue
{
public:
	Object() : GenericValue(GenericValueType::Object) {}
};

class NamedObject : public GenericValue
{
public:
	NamedObject(std::string& name) 
		: GenericValue(GenericValueType::NamedObject) 
		, name(name)
	{}

	std::string GetName() { return name; }

private:
	std::string name;
};

class Value : public GenericValue
{
public:
	Value(std::string& value)
		: GenericValue(GenericValueType::Value)
		, value(value)
	{
	}

	std::string GetValue() { return value; }

private:
	std::string value;
};

class KeyValue : public GenericValue
{
public:
	KeyValue(std::string& key, std::string& value)
		: GenericValue(GenericValueType::KeyValue)
		, key(key)
		, value(value)
	{
	}

	std::string GetKey() { return key; }
	std::string GetValue() { return value; }

private:
	std::string key;
	std::string value;
};

class KeyValueArray : public GenericValue
{
public:
	KeyValueArray(std::string& key)
		: GenericValue(GenericValueType::KeyValueArray)
		, key(key)
	{}

	std::string GetKey() { return key; }

private:
	std::string key;
};

class SonDocument : public GenericValue
{
public:
	SonDocument() : GenericValue(GenericValueType::Document) {}

	std::vector<GenericValue*> getObjectArray(std::string key);
	std::string getStringValue(std::string key, std::string defaultValue = "");
	int getIntValue(std::string key, int defaultValue = 0);
	float getFloatValue(std::string key, float defaultValue = 0.0f);
	double getDoubleValue(std::string key, double defaultValue = 0.0);
};
