#include "SonParser.h"

#include <algorithm>
#include <strstream>

std::vector<GenericValue*> arrayValues;

enum class ParserState
{
	None,
	InLineComment,
	InBlockComment,
	InObject,
	InArray,
	Token,
	QuotedToken,
};

SonParser::SonParser()
	: source(nullptr)
	, length(0)
	, hasParseErrors(false)
	, parseError("")
{
}

SonParser::SonParser(const std::string& srcStr)
{
	SetSource(srcStr);
}

SonParser::SonParser(const char* source, size_t& length)
{
	SetSource(source, length);
}

void SonParser::SetSource(const std::string& srcStr)
{
	source = srcStr.data();
	length = srcStr.size();

	ResetError();
}

void SonParser::SetSource(const char* source, size_t& length)
{
	SonParser::source = source;
	SonParser::length = length;

	ResetError();
}

inline bool IsSkip(char& a)
{
	return (a == ' ' || a == '\t' || a == '\r' || a == '\n');
}

inline bool IsTerminal(char& a, char& b)
{
	return (a >= 'a' && a <= 'z') || (a >= 'A' && a <= 'Z') || (a >= '0' && a <= '9') || a == '.' || a == '-' || a == '_' || (a == '/' && b != '/' && b != '*') || a == '\\';
}

void SonParser::Parse(GenericValue& document)
{
	std::stack<ParserState> stateStack;
	Parse(document, stateStack);
}

void SonParser::Parse(GenericValue& document, const std::string& source)
{
	SetSource(source);
	Parse(document);
}

void SonParser::Parse(GenericValue& document, const char* source, size_t& length)
{
	SetSource(source, length);
	Parse(document);
}

void SonParser::Parse(GenericValue& document, std::stack<ParserState>& stateStack)
{
	if (length <= 0)
	{
		hasParseErrors = true;
		parseError = "nothing to parse";
		return;
	}

	char a, b;
	size_t tokenStart = 0, objectStart = 0, arrayStart = 0;
	int tokenEnd = 0;
	std::string key, value;

	// The stack for the states is necessary for nested comments:
	//  when a comment is inside an array e.g. we need to go back to the previous state
	//  not to "None" for continue parsing the array and not starting all over. this can
	//  be easily realized by a stack of states.
	stateStack.push(ParserState::None);

	for (size_t i = 0; i <= length; i++)
	{
		a = source[i];

		switch (stateStack.top())
		{
			case ParserState::None:
				b = i<length-1 ? source[i+1] : '\0';

				if (a == '/' && b == '/') { stateStack.push(ParserState::InLineComment); i++; continue; }
				if (a == '/' && b == '*') { stateStack.push(ParserState::InBlockComment); i++; continue; }
				if (a == '{') { objectStart = i+1; stateStack.push(ParserState::InObject); continue; }
				if (a == '[') { arrayStart = i+1; stateStack.push(ParserState::InArray); continue; }
				if ( IsSkip(a) ) { continue; }

				if ( IsTerminal(a,b) ) { tokenStart = i; tokenEnd = -1; stateStack.push(ParserState::Token); continue; }
				if ( a == '"' ) { tokenStart = i; tokenEnd = -1; stateStack.push(ParserState::QuotedToken); continue; }

				// error handling
				if (a == '*' && b == '/') { SetError( "parse error: unexpected '*/' (end of block comment) at position ", i ); return; }
				if (a == '}')             { SetError( "parse error: unexpected '}' at position ", i); return; }
				if (a == ']')             { SetError( "parse error: unexpected ']' at position ", i); return; }

				break;
			case ParserState::InLineComment:
				if (a == '\r' || a =='\n')
				{
					b = i<length-1 ? source[i+1] : '\0';

					if (b == '\r' || b == '\n') i++;
					stateStack.pop();
				}

				break;
			case ParserState::InBlockComment:
				b = i<length-1 ? source[i+1] : '\0';
				if (a == '*' && b == '/') { stateStack.pop(); i++; }

				break;
			case ParserState::InObject:
				{
					size_t curlyBracketOpenCount = 1;
					size_t curlyBracketCloseCount = 0;
					size_t j = objectStart;

					while (curlyBracketOpenCount - curlyBracketCloseCount > 0 && j < length)
					{
						if (source[j] == '{') curlyBracketOpenCount++;
						if (source[j] == '}') curlyBracketCloseCount++;
						j++;
					}

					const char* startIdx = &source[objectStart];
					size_t length = j - objectStart - 2;

					GenericValue* subDocument;
					if (key.size() > 0) 
					{
						subDocument = new NamedObject(key); 
						key.resize(0);
					}
					else 
					{
						subDocument = new Object();
					}
					SonParser subDocumentParser(startIdx, length);
					subDocumentParser.Parse(*subDocument, stateStack);

					if (subDocumentParser.HasParseErrors())
					{
						SetError(subDocumentParser.GetParseError(), j);
						return;
					}

					document.push_back(subDocument);
					stateStack.pop();

					i += length + 1;
				}
				break;
			case ParserState::InArray:
				{
					size_t squareBracketOpenCount = 1;
					size_t squareBracketCloseCount = 0;
					size_t j = arrayStart;

					while (squareBracketOpenCount - squareBracketCloseCount > 0 && j < length)
					{
						if (source[j] == '[') squareBracketOpenCount++;
						if (source[j] == ']') squareBracketCloseCount++;
						j++;
					}

					const char* startIdx = &source[arrayStart];
					size_t length = j - arrayStart - 2;

					GenericValue* subDocument = new KeyValueArray(key);
					key.resize(0);
					SonParser subDocumentParser(startIdx, length);
					subDocumentParser.Parse(*subDocument, stateStack);

					if (subDocumentParser.HasParseErrors())
					{
						SetError(subDocumentParser.GetParseError(), j);
						return;
					}

					document.push_back(subDocument);
					stateStack.pop();

					i += length + 1;
				}
				break;
			case ParserState::QuotedToken:
				if (a == '\r' || a == '\n' || i == length)
				{
					SetError("You started a quoted TOKEN but couldn't find a closing quotation mark", tokenStart);
				}

				if (a == '"')
				{
					tokenStart++;
					tokenEnd = i;
					stateStack.pop();
					stateStack.push(ParserState::Token);
				}
				break;
			case ParserState::Token:
				bool quoted = tokenEnd > -1;
				b = i<length-1 ? source[i+1] : '\0';

				// KEY is terminated by a colon ":"
				// -> expect a VALUE, ARRAY or OBJECT
				// VALUE is NOT terminated by a colon ":"

				if ( a == ':' )
				{
					// found a key, value will follow as next token
					if (!quoted) tokenEnd = i;
					size_t tokenLength = tokenEnd - tokenStart;
					key.resize(tokenLength);
					memcpy((void*)key.data(), (void*)&source[tokenStart], tokenLength);
					if (!quoted) key.erase( std::remove_if( key.begin(), key.end(), ::isspace), key.end() );
					stateStack.pop();
				}
				else if (key.size() > 0 && a == '/' && (b == '/' || b == '*') && !quoted)
				{
					// found a value -> push key/value
					tokenEnd = i - 1;
					size_t tokenLength = tokenEnd - tokenStart;
					value.resize(tokenLength);
					memcpy((void*)value.data(), (void*)&source[tokenStart], tokenLength);
					if (!quoted) value.erase( std::remove_if( value.begin(), value.end(), ::isspace), value.end() );
					stateStack.pop();
					document.push_back(new KeyValue(key,value));
					key.resize(0);
					value.resize(0);
					i++;
					if (b == '/')
					{
						stateStack.push(ParserState::InLineComment);
					}
					else
					{
						stateStack.push(ParserState::InBlockComment);
					}
				}
				else if (key.size() > 0 && (a == ',' || a == '\r' || a == '\n' || i == length))
				{
					// found a value -> push key/value
					if (!quoted) tokenEnd = i;
					size_t tokenLength = tokenEnd - tokenStart;
					value.resize(tokenLength);
					memcpy((void*)value.data(), (void*)&source[tokenStart], tokenLength);
					if (!quoted) value.erase( std::remove_if( value.begin(), value.end(), ::isspace), value.end() );
					stateStack.pop();
					document.push_back(new KeyValue(key,value));
					key.resize(0);
					value.resize(0);
				}
				else if (key.size() == 0 && (a == ',' || a == '\r' || a == '\n' || i == length))
				{
					// found a value -> push value (only allowed in ARRAY)
					if (document.GetType() != GenericValueType::KeyValueArray)
					{
						SetError("VALUE is allowed as subelement of ARRAY only. Value starts at position: ", tokenStart);
						return;
					}

					if (!quoted) tokenEnd = i;
					size_t tokenLength = tokenEnd - tokenStart;
					value.resize(tokenLength);
					memcpy((void*)value.data(), (void*)&source[tokenStart], tokenLength);
					if (!quoted) value.erase( std::remove_if( value.begin(), value.end(), ::isspace), value.end() );
					stateStack.pop();
					document.push_back(new Value(value));
					value.resize(0);
				}

				break;
		}
	}

	stateStack.pop();
}

bool SonParser::HasParseErrors()
{
	return hasParseErrors;
}

std::string SonParser::GetParseError()
{
	return parseError;
}

void SonParser::ResetError()
{
	hasParseErrors = false;
	parseError = "";
}

void SonParser::SetError(const char* error, size_t& position)
{
	SetError( std::string(error), position );
}

void SonParser::SetError(std::string& error, size_t& position)
{
	parseError = error + std::to_string(position);
	hasParseErrors = true;
}
