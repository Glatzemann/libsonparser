#include "GenericValue.h"

std::string GenericValue::getStringValue(std::string key, std::string defaultValue)
{
	for (size_t i = 0; i < this->size(); i++)
	{
		auto kv = this->at(i);
		if (   kv->GetType() == GenericValueType::KeyValue
			&& key.compare( ((KeyValue*)kv)->GetKey() ) == 0
			)
		{
			return ((KeyValue*)kv)->GetValue();
		}
	}

	return defaultValue;
}

int GenericValue::getIntValue(std::string key, int defaultValue)
{
	return std::stoi( getStringValue(key, std::to_string(defaultValue)) );
}

float GenericValue::getFloatValue(std::string key, float defaultValue)
{
	return std::stof( getStringValue(key, std::to_string(defaultValue)) );
}

double GenericValue::getDoubleValue(std::string key, double defaultValue)
{
	return std::stod( getStringValue(key, std::to_string(defaultValue)) );
}

std::vector<GenericValue*> GenericValue::getObjectArray(std::string key)
{
	std::vector<GenericValue*> result;

	for (auto kv : *this)
	{
		if (kv->GetType() == GenericValueType::KeyValueArray && key.compare( ((KeyValueArray*)kv)->GetKey() ) == 0 )
		{
			for (unsigned int i = 0; i < kv->size(); i++)
			{
				result.push_back(kv->at(i));
			}
		}
	}

	return result;
}


std::vector<GenericValue*> SonDocument::getObjectArray(std::string key)
{
	std::vector<GenericValue*> result;

	for (auto kv : *this)
	{
		if (kv->GetType() == GenericValueType::KeyValueArray && key.compare( ((KeyValueArray*)kv)->GetKey() ) == 0 )
		{
			for (unsigned int i = 0; i < kv->size(); i++)
			{
				result.push_back(kv->at(i));
			}
		}
	}

	return result;
}

std::string SonDocument::getStringValue(std::string key, std::string defaultValue)
{
	for (auto kv : *this)
	{
		if (   kv->GetType() == GenericValueType::KeyValue
			&& key.compare( ((KeyValue*)kv)->GetKey() ) == 0
			)
		{
			return ((KeyValue*)kv)->GetValue();
		}
	}

	return defaultValue;
}

int SonDocument::getIntValue(std::string key, int defaultValue)
{
	return std::stoi( getStringValue(key, std::to_string(defaultValue)) );
}

float SonDocument::getFloatValue(std::string key, float defaultValue)
{
	return std::stof( getStringValue(key, std::to_string(defaultValue)) );
}

double SonDocument::getDoubleValue(std::string key, double defaultValue)
{
	return std::stod( getStringValue(key, std::to_string(defaultValue)) );
}
