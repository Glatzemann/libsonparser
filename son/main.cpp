#include <iostream>
#include <fstream>
#include <time.h>

#include "SonParser.h"

void printGenericValue(GenericValue* value, int tabs = 0)
{
	for (int i = 0; i < tabs; i++)
	{
		std::cout << "\t";
	}

	switch (value->GetType())
	{
		case GenericValueType::KeyValue:
			std::cout << "KEY: '" << ((KeyValue*)value)->GetKey().data() << "' VALUE: '" << ((KeyValue*)value)->GetValue().data() << "'" << std::endl;
			break;
		case GenericValueType::Value:
			std::cout << "VALUE: '" << ((Value*)value)->GetValue().data() << "'" << std::endl;
			break;
		case GenericValueType::KeyValueArray:
			std::cout << "Array KEY: '" << ((KeyValueArray*)value)->GetKey().data() << "'" << std::endl;

			for (size_t i = 0 ; i < value->size(); i++)
			{
				GenericValue* v = value->at(i);
				printGenericValue(v, tabs+1);
			}

			break;
		case GenericValueType::NamedObject:
			std::cout << "OBJECT (name: " << ((NamedObject*)value)->GetName() << "): " << std::endl;

			for (size_t i = 0 ; i < value->size(); i++)
			{
				GenericValue* v = value->at(i);
				printGenericValue(v, tabs+1);
			}

			break;
		case GenericValueType::Object:
			std::cout << "OBJECT: " << std::endl;

			for (size_t i = 0 ; i < value->size(); i++)
			{
				GenericValue* v = value->at(i);
				printGenericValue(v, tabs+1);
			}

			break;

	}
}

int main(int argc, char* argv[])
{
	std::string filename(argv[1]);

	std::ifstream file(filename.c_str(), std::ios::in | std::ios::binary | std::ios::ate);
	if (file.is_open())
	{
		file.seekg(0, std::ios::end);
		size_t size = static_cast<size_t>(file.tellg());
		char* contents = new char[size+1];
		file.seekg(0, std::ios::beg);
		file.read(contents, size);
		file.close();

		clock_t t = clock();

		SonParser parser(contents, size);

		SonDocument document;
		parser.Parse(document);
		printf("\nTime taken for parsing: %.4fs\n\n\n", (float)(clock() - t)/CLOCKS_PER_SEC);

		if (parser.HasParseErrors())
		{
			std::cout << "parse error: " << parser.GetParseError() << std::endl;
		}
		else
		{
			for (auto v : document)
			{
				printGenericValue(v);
			}
		}
	}
	else
	{
		std::cerr << "couldn't open file '" << filename << "'" << std::endl;
	}

	std::cout << std::endl << std::endl;
	system("pause");

	return 0;
}
